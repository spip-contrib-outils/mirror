# mirror.php

Un script pour clone&pull tous les projets de git.spip.net

```
php mirror.php
```

Le script interroge l'API Gitea pour avoir la liste des repositories de chaque organisation, et met a jour le depot si il a ete modifie depuis le dernier pull.
Par défaut le clone/pull se fait depuis l'URL https.

Pour cloner/pull en ssh :
```
php mirror.php --ssh
```


Un cache de 10mn est utilisé sur l'API Gitea pour eviter de la solliciter à chaque appel si le script est appelé trop souvent.

Les repositories en erreur ou vide sont détectés et à la fin on a un bilan final :

```
1201 Repositories au total

5 Repositories vides
- https://git.spip.net/spip-contrib-squelettes/html5up_escape_velocity.git
- https://git.spip.net/spip-contrib-extensions/eva_geometrie.git
- https://git.spip.net/spip-contrib-extensions/diogene_agenda.git
- https://git.spip.net/spip-contrib-extensions/formidable_identification.git
- https://git.spip.net/spip/bigup-bis.git

1 Repositories mis a jour
- https://git.spip.net/spip-contrib-outils/gitea_mirror.git

```